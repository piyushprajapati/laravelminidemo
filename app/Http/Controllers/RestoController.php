<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;
use App\User;
use Session;
use Crypt;

class RestoController extends Controller
{
    public function index(){
        return view('home');
    }
    public function list(){
        $data = Restaurant::all(); 
        return view('list',['data'=>$data]);
    }
    
    public function add(Request $req){
        // return $req->input();
        $resto = new Restaurant;
        $resto->name = $req->input('name'); 
        $resto->email = $req->input('email'); 
        $resto->address = $req->input('address'); 
        $resto->save();
        $req->session()->flash('status','Restaurant Added Successfully');
        return redirect('list');
    }

    public function delete($id){
        // return $id;
        Restaurant::find($id)->delete();
        Session::flash('status','Restaurant Deleted Successfully');
        return redirect('list');
    }
    
    public function edit($id){
        // return $id;
        $data = Restaurant::find($id);
        // Session::flash('status','Restaurant Deleted Successfully');
        return view('edit',['data'=>$data]);
    }
    
    public function update(Request $req){
        // return $id;
        $resto = Restaurant::find($req->input('id'));
        $resto->name = $req->input('name'); 
        $resto->email = $req->input('email'); 
        $resto->address = $req->input('address'); 
        $resto->save();
        $req->session()->flash('status','Restaurant Updated Successfully');
        return redirect('list');
    }

    public function register(Request $req){
        // echo Crypt::encrypt('piyush');
        $user = new User;
        $user->name = $req->input('name'); 
        $user->email = $req->input('email'); 
        $user->password = Crypt::encrypt($req->input('password')); 
        $user->phone = $req->input('phone'); 
        $user->save();
        $req->session()->put('user',$req->input('name'));
        return redirect('/');
    }
}
