@extends('layout')

@section('content')
<h1>Register User</h1>    

<form action="register" method="post">
    @csrf 
    <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" class="form-control" placeholder="Enter Name">          
    </div>
    <div class="form-group">
          <label>Email</label>
          <input type="email" name="email" class="form-control" placeholder="Enter Email">          
    </div>
    <div class="form-group">
          <label>Password</label>
          <input type="password" name="password" class="form-control" placeholder="Enter Password">          
    </div>
    <div class="form-group">
          <label>Phone No.</label>
          <input type="text" name="phone" class="form-control" placeholder="Enter Phone Number">          
    </div>
       
        <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
