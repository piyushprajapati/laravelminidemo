@extends('layout')

@section('content')
<h1>Restaurant List</h1>    
{{-- {{print_r($data)}} --}}

@if (Session::get('status'))
<div class="alert alert-success">
    {{Session::get('status')}}
</div>
@endif

<table class="table  table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Email</th>
        <th>Address</th>
        <th>Created Date</th>
        <th>Action</th>
    </tr>
    @foreach ($data as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->address}}</td>
            <td>{{$item->created_at}}</td>            
            <td>
                <a href="edit/{{$item->id}}"><button class="btn btn-primary">Edit</button></a>
                <a href="delete/{{$item->id}}"><button class="btn btn-danger">Delete</button></a>
            </td>
        </tr>
@endforeach
</table>

@endsection
